bin = @["icalendar_actor"]
license = "Unlicense"
srcDir = "src"
version = "20230825"
requires "nim", "syndicate"
