# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import std/[json, sequtils, tables, uri]
import preserves, preserves/jsonhooks, syndicate, syndicate/relays

import ./icalendar_components

type BootArgs {.preservesDictionary.} = object
  dataspace: Cap

proc importCalendar(location: string): Component =
  let uri = parseUri(location)
  if uri.scheme != "file":
    raiseAssert """only supported calendar schema is "file" ignoring """ & location
  else:
    var
      js = parseFile(uri.path)
      pr = jsonhooks.toPreservesHook(js)
    if not result.fromPreserves(pr):
      # TODO: convert directly from JsonNode
      raise newException(ValueError, "Preserves was not a valid iCalendar component")
    if result.label != "vcalendar":
      raise newException(ValueError, "Preserves was not a valid vcalendar @component")

proc toPreserveHook(prop: Property): Value =
  initRecord(prop.label,
      prop.parameters.toPreserves,
      prop.`type`.toPreserves,
      prop.values.toPreserves,
    )

proc toPreserveHook(comp: Component): Value =
  initRecord(comp.label,
      comp.properties.toPreserves,
      comp.components.toPreserves,
    )

proc serve(turn: var Turn; url: string): Cap =
  let ds = newDataspace(turn)
  let calendar = importCalendar(url)
  run(turn.facet) do (turn: var Turn):
    for prop in calendar.properties:
      discard publish(turn, ds, prop)
    for comp in calendar.components:
      discard publish(turn, ds, comp)
  ds

runActor("icalendar_actor") do (root: Cap; turn: var Turn):
  connectStdio(turn, root)
  stderr.writeLine "connected stdio to ", root
  during(turn, root, ?:BootArgs) do (ds: Cap):
    stderr.writeLine "got dataspace at ", ds
    during(turn, ds, ?Observe(pattern: !CalendarDataspace) ?? {0: grabLit()}) do (url: string):
      let cap = serve(turn, url)
      stderr.writeLine "serving ", url, " at ", cap
      discard publish(turn, ds, initRecord("calendar", url.toPreserves, cap.embed))
