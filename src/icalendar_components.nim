
import
  preserves, std/tables

type
  CalendarDataspace* {.preservesRecord: "calendar".} = object
    `url`*: string
    `dataspace`* {.preservesEmbedded.}: EmbeddedRef

  Property* {.preservesTuple.} = object
    `label`*: string
    `parameters`*: Parameters
    `type`*: string
    `values`* {.preservesTupleTail.}: seq[Value]

  Parameters* = Table[Symbol, string]
  Component* {.preservesTuple.} = object
    `label`*: string
    `properties`*: seq[Property]
    `components`*: seq[Component]

proc `$`*(x: CalendarDataspace | Property | Parameters | Component): string =
  `$`(toPreserves(x))

proc encode*(x: CalendarDataspace | Property | Parameters | Component): seq[byte] =
  encode(toPreserves(x))
